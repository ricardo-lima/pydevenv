import base64
import json
import os
from typing import Any


JSONData = None | bool | int | float | str | list[Any] | dict[str, Any]
Path = str | os.PathLike


def b64e(s: str) -> str:
    return base64.b64encode(s.encode()).decode()


def b64d(s: bytes) -> str:
    return base64.b64decode(s).decode()


def read_file(file: Path) -> Any:
    with open(file, "r", encoding="utf-8") as fp:
        return fp.read()


def write_file(file: Path, data: Any) -> None:
    with open(file, "x", encoding="utf-8") as fp:
        fp.write(data)
    return None


def read_json_file(file: Path) -> JSONData:
    with open(file, "r", encoding="utf-8") as fp:
        return json.load(fp)


def write_json_file(file: Path, data: JSONData) -> None:
    with open(file, "x", encoding="utf-8") as fp:
        json.dump(data, fp, indent=4, ensure_ascii=False, sort_keys=False)
    return None

